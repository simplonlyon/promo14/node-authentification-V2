# NODE AUTHENTIFICATION VERSION CLIENT SERVER

* Reprise (clone) de l'atelier Node-authentification
* Création d'un dossier "server" pour la partie back-end
* Installation des node module pour le back
* Dossier config avec les clés privée + publique (voir [gitlab](https://gitlab.com/simplonlyon/promo14/node-authentication))
* Vérification de la liaison avec sa bdd + test de l'API et ses routes
* Création d'un **client** avec ``npx create-react-app client``
* Création d'un formulaire d'inscription avec utilisation d'Axios

