import React, { useState } from "react";
import './App.css';
import Axios from 'axios'

const App = () => {
  const [registerEmail, setRegisterEmail] =useState("")
  const [registerPassword, setRegisterPassword] =useState("")
  const register = () => {
    Axios({
      method :"POST",
      data : {
        email : registerEmail,
        password : registerPassword,
      },
      withCredentials :true, 
      url :"http://localhost:8000/api/user"
    })
  }
  return (
    <div className="App">
      <div>
        <h1>Register</h1>
        <input
        type = "email"
        placeholder = "email"
        onChange={(e) => setRegisterEmail(e.target.value)}
         />
        <input
        type="password"
        placeholder = "password"
        onChange={(e) => setRegisterPassword(e.target.value)}
         />
         <button onClick={register}>Submit</button>
      </div>
    </div>
  );
}

export default App;
