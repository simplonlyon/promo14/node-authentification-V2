
## How to Use
1. Créer une base de données et importer le fichier dedans (exemple: `mysql -u simplon -p node_authentication < db.sql`)
2. `npm i`
3. Générer une paire de clef privée/clef publique dans un dossier config (qu'il faut créer)
```
ssh-keygen -t rsa -P "" -b 4096 -m PEM -f config/id_rsa
ssh-keygen -e -m PEM -f config/id_rsa > config/id_rsa.pub
```
4. `npm start` 
