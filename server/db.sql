DROP TABLE IF EXISTS user;
CREATE TABLE user(  
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    email VARCHAR (255) NOT NULL UNIQUE,
    password VARCHAR (255) NOT NULL,
    role VARCHAR (255) NOT NULL
) default charset utf8 comment '';